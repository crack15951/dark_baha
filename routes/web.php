<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['web']], function () {
    Route::pattern('id', '[0-9]+');

    // 首頁 
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
    Route::get('home', ['as' => 'home', 'uses' => 'HomeController@index']);
});